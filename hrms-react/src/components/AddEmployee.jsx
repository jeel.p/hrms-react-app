import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import axios from "axios";
import { Row, Col, CardBody, Card, Alert, Container, Form, Input, FormFeedback, Label, Button, CardTitle, Placeholder } from "reactstrap";

import "./UserRegistation.css";
import { string } from "yup";
const AddEmployee = () => {

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [number, setNumber] = useState("");
    const [password, setPassword] = useState("");
    const [designation, setDesignation] = useState("admin");
    const [joining_date, setJoining_date] = useState();
    const { register, handleSubmit } = useForm();

    const navigate = useNavigate();

    let handleRegister = async () => {
        console.log("email",email)
        // e.preventDefault();
        const response = await axios.post("http://localhost:3001/admin/add", { name, email, password, joining_date, designation });
        // console.log("hello") 
        navigate("/")
    };


    return (
        <>

            <div className="col-md-4 col-md-offset-4 center">
                <h2>Register:</h2> <br />
                <form onSubmit={handleSubmit(handleRegister)}>
                    <div className="mb-3">
                        <label for="name" className="form-label"> Name</label>
                        <input type="text" className="form-control" name="name" id="name" Placeholder="Name" onChange={(e) => { setName(e.target.value); }}
                            {...register('name', { required: true, type: string, minLength: 2, maxLength: 30, message: "Enter Proper Name" })} />
                        {/* {errors.name && <p>{errors.name.message}</p>} */}
                    </div>

                    <div className="mb-3">
                        <label for="email" className="form-label">
                            Email
                        </label>
                        <input type="email" className="form-control" name="email" id="email" Placeholder="Email" onChange={(e) => { setEmail(e.target.value); }}
                            {...register('email', { required: true })} />
                    </div>

                    <div className="mb-3">
                        <label for="password" className="form-label">
                            Password
                        </label>
                        <input type="password" className="form-control" name="password" id="password" Placeholder="Enter Your Password" onChange={(e) => { setPassword(e.target.value); }}
                            {...register('password', { required: true })} />
                    </div>

                    <div className="mb-3">
                        <label for="designation" className="form-label">
                            Designation
                        </label>
                        <select className="form-select" aria-label="Default select example" name="designation" id="designation" value={designation} onChange={(e) => { setDesignation(e.target.value); }} >
                            <option value="employee">Employee</option>
                            <option value="admin">Admin</option>
                            <option value="project_manager">Project manager</option>
                            <option value="account_manager">Account manager</option>
                        </select>
                    </div>

                    <div className="mb-3">
                        <label for="joining_date" className="form-label">
                            Join Date
                        </label>
                        <input type="date" className="form-control" name="joining_date" id="joining_date" value={joining_date} onChange={(e) => { setJoining_date(e.target.value); }} {...register('joining_date', { required: true })} />
                    </div>

                    <br />

                    <input type="submit" className="btn btn-outline-primary" />

                </form>
            </div>
        </>

    );
}

export default AddEmployee;