import React, { useState } from "react";
import { useNavigate, useLocation } from 'react-router-dom';
import axios from "axios";
import "./UserRegistation.css";
const UserRegistation = () => {

    const location = useLocation();
    const { user } = location.state;

    const [name, setName] = useState(user.name);
    const [email, setEmail] = useState(user.email);
    const [password, setPassword] = useState(user.password);
    const [joining_date, setJoining_date] = useState(user.joining_date);
    const [designation, setDesignation] = useState(user.designation);
    const [contact_number, setContact_number] = useState("");
    const [skills, setSkills] = useState("");
    const [address, setAddress] = useState("");
    const [gender, setGender] = useState("male");
    const [dob, setDob] = useState();
    const [bank_name, setBank_name] = useState("");
    const [bank_account, setBank_account] = useState("");
    const [ifsc, setIfsc] = useState("");
    const [aadhar_card, setAadhar_card] = useState("");
    const [pan_card, setPan_card] = useState("");
    const [user_photo, setUser_photo] = useState("");

    const navigate = useNavigate();

    let handleSubmit = async (e) => {

        e.preventDefault();

        let formdata = new FormData();
        formdata.append("name", name);
        formdata.append("email", email);
        formdata.append("password", password);
        formdata.append("joining_date", joining_date);
        formdata.append("designation", designation);
        formdata.append("DOB", dob);
        formdata.append("gender", gender);
        formdata.append("bank_name", bank_name);
        formdata.append("bank_account", bank_account);
        formdata.append("ifsc", ifsc);
        formdata.append("aadhar_card", aadhar_card);
        formdata.append("pan_card", pan_card);
        formdata.append("user_photo", user_photo);
        formdata.append("address", address);
        formdata.append("skills", skills);
        formdata.append("contact_number", contact_number);
        // console.log(formdata);

        const response = await axios.post(`http://localhost:3001/user/${user._id}`, formdata, {
            headers: {
                accept: "application/json",
                "Accept-Language": "en-US,en;q=0.8",
                "Content-Type": "multipart / form - data",
            }
        });

        navigate("/")

    };
    return (
        <div className="col-md-4 col-md-offset-4 center">
            <h2>Register:</h2> <br />
            <form>

                <div className="mb-3">
                    <label for="name" className="form-label"> Name</label>
                    <input type="text" className="form-control" name="name" id="name" value={name} onChange={(e) => { setName(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="email" className="form-label">
                        Email
                    </label>
                    <input type="email" className="form-control" name="email" id="email" value={email} onChange={(e) => { setEmail(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="password" className="form-label">
                        Password
                    </label>
                    <input type="password" className="form-control" name="password" id="password" onChange={(e) => { setPassword(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="designation" className="form-label">
                        Designation
                    </label>
                    <select className="form-select" aria-label="Default select example" name="designation" id="designation" value={designation} onChange={(e) => { setDesignation(e.target.value); }} >
                        <option value="employee">Employee</option>
                        <option value="admin">Admin</option>
                        <option value="project_manager">Project manager</option>
                        <option value="account_manager">Account manager</option>
                    </select>
                </div>



                <div className="mb-3">
                    <label for="contact_number" className="form-label">
                        Contact_number
                    </label>
                    <input type="number" className="form-control" name="contact_number" id="contact_number" onChange={(e) => { setContact_number(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="skills" className="form-label">
                        Skills
                    </label>
                    <input type="text" className="form-control" name="skills" id="skills" onChange={(e) => { setSkills(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="address" className="form-label">
                        Address
                    </label>
                    <input type="text" className="form-control" name="address" id="address" onChange={(e) => { setAddress(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="gender" className="form-label">
                        Gender
                    </label>
                    <select className="form-select" aria-label="Default select example" name="gender" id="gender" value={gender} onChange={(e) => { setGender(e.target.value); }}>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>

                <div className="mb-3">
                    <label for="DOB" className="form-label">
                        Birth Date
                    </label>
                    <input type="date" className="form-control" name="DOB" id="DOB" value={dob} onChange={(e) => { setDob(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="bank_name" className="form-label">
                        Bank name
                    </label>
                    <input type="text" className="form-control" name="bank_name" id="bank_name" onChange={(e) => { setBank_name(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="bank_account" className="form-label">
                        Bank account
                    </label>
                    <input type="number" className="form-control" name="bank_account" id="bank_account" onChange={(e) => { setBank_account(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="ifsc" className="form-label">
                        IFSC
                    </label>
                    <input type="text" className="form-control" name="ifsc" id="ifsc" onChange={(e) => { setIfsc(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <label for="aadhar_card" className="form-label">
                        Aadhar Card
                    </label>
                    <input className="form-control" type="file" id="aadhar_card" name="aadhar_card" onChange={(e) => { setAadhar_card(e.target.files[0]) }} />
                </div>

                <div className="mb-3">
                    <label for="pan_card" className="form-label">
                        Pan Card
                    </label>
                    <input className="form-control" type="file" id="pan_card" name="pan_card" onChange={(e) => { setPan_card(e.target.files[0]); }} />
                </div>

                <div className="mb-3">
                    <label for="user_photo" className="form-label">
                        User Photo
                    </label>
                    <input className="form-control" type="file" id="user_photo" name="user_photo" onChange={(e) => { setUser_photo(e.target.files[0]); }} />
                </div>

                <br />

                <input type="submit" className="btn btn-outline-primary" onClick={handleSubmit} />

            </form>
        </div>
    );
};

export default UserRegistation;
