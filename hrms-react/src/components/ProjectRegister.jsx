import React from "react";
export default function ProjectRegister() {
    return (
        <div className="col-md-4 col-md-offset-4 center">
            <h1>Project</h1>
            <form action="/sql/login" method="POST">
                <div className="mb-3">
                    <label for="name" className="form-label">
                        Project Name
                    </label>
                    <input type="text" className="form-control" name="name" id="name" placeholder="Enter Project Name" required />
                </div>
                <div className="mb-3">
                    <label for="start_date" className="form-label">
                        Start Date
                    </label>
                    <input type="date" className="form-control" name="start_date" id="start_date" placeholder="Enter Project Name" required />
                </div>
                <div className="mb-3">
                    <label for="status" className="form-label">
                        Status
                    </label>
                    <select className="form-select" aria-label="Default select example" name="status" id="status">
                        <option value="Pending" selected>
                            Pending
                        </option>
                        <option value="Completed">Completed</option>
                        {/* <option value="project_manager">Project manager</option>
            <option value="account_manager">Account manager</option> */}
                    </select>
                </div>
                <div className="mb-3">
                    <label for="end_date" className="form-label">
                        End Date
                    </label>
                    <input
                        type="date" className="form-control" name="end_date" id="end_date" />
                </div>

                <div className="mb-3">
                    <label for="deadline" className="form-label">
                        DeadLine
                    </label>
                    <input type="date" className="form-control" name="deadline" id="deadline" />
                </div>
                <div className="mb-3">
                    <label for="description" className="form-label">
                        Project Description
                    </label>
                    <textarea className="form-control" name="description" id="description" placeholder="Enter Project Description" required />
                </div>

                <div className="mb-3">
                    <label for="estimated_duration" className="form-label">
                        Estimated Days
                    </label>
                    <input type="number" className="form-control" name="estimated_duration" id="estimated_duration" />
                </div>

                <button type="submit" className="btn btn-outline-primary">
                    Add
                </button>
            </form>
        </div>
    );
}