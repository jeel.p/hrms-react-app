import React from 'react'
import { useLocation } from 'react-router-dom';
const Welcome = () => {
    const location=useLocation();
    const {user}=location.state;
    let user_photo="";
    if(user.user_photo===undefined){
        console.log("no photo")
        user_photo="http://localhost:3001/uploads/documents/default.png"
    }
    else{
        user_photo="http://localhost:3001"+user.user_photo
    }
    return ( 
        <div>
            <h1>Welcome {user.name}</h1> <br />
            <img src={user_photo} height="100px" width="100px" alt="" />
        </div>
     );
}
 
export default Welcome;