import React from 'react'
import "./UserRegistation.css"
const Leave = () => {
    return (
        <div className="col-md-4 col-md-offset-4 center">
            <h2>Leave:</h2> <br />
            <form>

                <div className="mb-3">
                    <label for="tital" className="form-label">Tital</label>
                    <input type="text" className="form-control" name="tital" id="tital" />
                </div>

                <div className="mb-3">
                    <label for="type" className="form-label">Type</label>
                    <input type="text" className="form-control" name="type" id="type" />
                </div>

                <div className="mb-3">
                    <label for="start_date" className="form-label">Start Date</label>
                    <input type="date" className="form-control" name="start_date" id="start_date" />
                </div>

                <div className="mb-3">
                    <label for="end_date" className="form-label">End Date</label>
                    <input type="date" className="form-control" name="end_date" id="end_date" />
                </div>

                <div className="mb-3">
                    <label for="reason" className="form-label">Reason</label>
                    <textarea className="form-control" id="reason" name="reason" />
                </div>

                <input type="submit" className='btn btn-outline-primary'></input>

            </form>
        </div>
    );
}

export default Leave;