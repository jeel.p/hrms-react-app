import React, { useState } from "react";
import axios from "axios";
export default function Task() {
    const [task, setTask] = useState({})
    // const [date, setDate] = useState(0);
    const [description, setDescription] = useState(0);

    const add = async (e) => {
        e.preventDefault();
        const t = {
            // date: date,
            description: description
        }
        // console.log("t",t);
        setTask(t);
        // console.log("task",task);
        await axios.post("http://localhost:3001/task", { description:t.description })
        // addDb();
    }
    // var addDb =async () => {
    // }

    return (
        <div className="col-md-4 col-md-offset-4 center">
            <h1>Task Summary</h1>
            <form>
                {/* <div className="mb-3">
                    <label for="user_id" className="form-label">user_id</label>
                    <input type="number" className="form-control" name="user_id" id="user_id" onChange={(e) => setUser_id(e.target.value)} />
                </div> */}
                {/* <div className="mb-3">
                    <label for="date" className="form-label">Date</label>
                    <input type="date" className="form-control" name="date" id="date" onChange={(e) => setDate(e.target.value)} />
                </div> */}
                <div className="mb-3">
                    <label for="description" className="form-label">
                        Today's task Summary
                    </label>
                    <textarea className="form-control" name="description" id="description" placeholder="Task Summary" onChange={(e) => setDescription(e.target.value)} required />
                </div>
                <button type="submit" className="btn btn-outline-primary" onClick={add}>
                    Add
                </button>
            </form>
        </div>
    );
}