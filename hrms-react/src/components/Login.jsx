import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from "axios"
import { withRouter, Link } from "react-router-dom";
import { useFormik } from "formik";
import { Row, Col, CardBody, Card, Alert, Container, Form, Input, FormFeedback, Label } from "reactstrap";
// import useDispatch from "react-redux";
import * as Yup from "yup";
import profile from "./assets/images/profile-img.png";
import logo from "./assets/images/logo.svg";

// import Container from 'reactstrap';



function Login() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [error, setError] = useState("")
    const navigate = useNavigate();
    document.title = "HRMS Login";
    // const dispatch = useDispatch();

    const validation = useFormik({
        // enableReinitialize : use this flag when initial values needs to be changed
        enableReinitialize: true,

        initialValues: {
            email: "" || '',
            password: "" || '',
        },
        validationSchema: Yup.object({
            email: Yup.string().required("Please Enter Your Email"),
            password: Yup.string().required("Please Enter Your Password"),
        }),

        // onSubmit: (values) => {
        //     dispatch(loginUser(values, props.history));
        // }
    });

    const handleSubmit = async () => {
        // e.preventDefault();
        let res = await axios.post("http://localhost:3001/login", { email, password })
        const person = res.data.person
        setEmail("");
        setPassword("");
        console.log(person);
        if (res.data.status) {
            if (person.contact_number === undefined || person.DOB === undefined) {
                navigate(`/${person.designation}/register`, { state: { user: person } })
            }
            else {
                navigate(`/${person.designation}/welcome`, { state: { user: person } })
            }
        }
        else {
            setEmail("");
            setPassword("");
            setError("Invalid email or password")
            navigate("/")
        }
    }

    return (
        <React.Fragment>
            <div className="home-btn d-none d-sm-block">
                <Link to="/" className="text-dark">
                    <i className="fas fa-home h2" />
                </Link>
            </div>
            <div className="account-pages my-5 pt-sm-5">
                <Container >
                    <Row className="justify-content-center">
                        <Col md={8} lg={6} xl={5}>
                            <Card className="overflow-hidden" >
                                <div className="bg-primary bg-soft" >
                                    <Row style={{ background: "#d4dbf9" }}>
                                        <Col xs={7}>
                                            <div className="text-primary p-4" >
                                                <h5 className="text-primary">Welcome Back !</h5>
                                                <p>Sign in to continue to HRMS.</p>
                                            </div>
                                        </Col>
                                        <Col className="col-5 align-self-end">
                                            <img src={profile} alt="" className="img-fluid" />
                                        </Col>
                                    </Row>
                                </div>
                                <CardBody className="pt-0">
                                    <div>
                                        <Link to="/" className="auth-logo-light">
                                            <div className="avatar-md profile-user-wid mb-4">
                                                <span className="avatar-title rounded-circle bg-light">
                                                    <img
                                                        src={logo}
                                                        alt=""
                                                        className="rounded-circle"
                                                        height="34"
                                                    />
                                                </span>
                                            </div>
                                        </Link>
                                    </div>
                                    <div className="p-2">
                                        <Form
                                            className="form-horizontal"
                                            onSubmit={(e) => {
                                                e.preventDefault();
                                                handleSubmit();
                                                return false;
                                            }}
                                        >
                                            {error ? <Alert color="danger">{error}</Alert> : null}

                                            <div className="mb-3">
                                                <Label className="form-label">Email</Label>
                                                <Input
                                                    name="email"
                                                    className="form-control"
                                                    placeholder="Enter email"
                                                    type="email"
                                                    onChange={(e) => {
                                                        setEmail(e.target.value);
                                                        // validation.handleChange()
                                                    }}
                                                    // onBlur={validation.handleBlur}
                                                    value={email || ""}
                                                    invalid={
                                                        validation.touched.email && validation.errors.email ? true : false
                                                    }
                                                />
                                                {validation.touched.email && validation.errors.email ? (
                                                    <FormFeedback type="invalid">{validation.errors.email}</FormFeedback>
                                                ) : null}
                                            </div>

                                            <div className="mb-3">
                                                <Label className="form-label">Password</Label>
                                                <Input
                                                    name="password"
                                                    value={password || ""}
                                                    type="password"
                                                    placeholder="Enter Password"
                                                    onChange={(e) => { setPassword(e.target.value) }}
                                                    // onBlur={validation.handleBlur}
                                                    invalid={
                                                        validation.touched.password && validation.errors.password ? true : false
                                                    }
                                                />
                                                {validation.touched.password && validation.errors.password ? (
                                                    <FormFeedback type="invalid">{validation.errors.password}</FormFeedback>
                                                ) : null}
                                            </div>


                                            <div className="mt-3 d-grid">
                                                <button
                                                    className="btn btn-outline-primary btn-block"
                                                    type="submit"
                                                >
                                                    Log In
                                                </button>
                                            </div>
                                        </Form>
                                    </div>
                                </CardBody>
                            </Card>

                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>


    );
}
export default Login;