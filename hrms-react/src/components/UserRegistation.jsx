import React, { useState } from "react";
import { useForm } from "react-hook-form"
import { useNavigate, useLocation } from 'react-router-dom';
import axios from "axios";
import { Card, Col, Container, Row, CardBody, CardTitle, Label, Button, Form, Input, InputGroup, } from "reactstrap";
import "./UserRegistation.css";
const UserRegistation = () => {

    const location = useLocation();
    const { user } = location.state;

    const { register, handleSubmit } = useForm();

    const [name, setName] = useState(user.name);
    const [email, setEmail] = useState(user.email);
    const [password, setPassword] = useState(user.password);
    const [joining_date, setJoining_date] = useState(user.joining_date);
    const [designation, setDesignation] = useState(user.designation);
    const [contact_number, setContact_number] = useState("");
    const [skills, setSkills] = useState("");
    const [address, setAddress] = useState("");
    const [gender, setGender] = useState("male");
    const [dob, setDob] = useState();
    const [bank_name, setBank_name] = useState("");
    const [bank_account, setBank_account] = useState("");
    const [ifsc, setIfsc] = useState("");
    const [aadhar_card, setAadhar_card] = useState("");
    const [pan_card, setPan_card] = useState("");
    const [user_photo, setUser_photo] = useState("");

    const navigate = useNavigate();

    let handleRegister = async () => {

        // console.log("hello")

        let formdata = new FormData();
        formdata.append("name", name);
        formdata.append("email", email);
        formdata.append("password", password);
        formdata.append("joining_date", joining_date);
        formdata.append("designation", designation);
        formdata.append("DOB", dob);
        formdata.append("gender", gender);
        formdata.append("bank_name", bank_name);
        formdata.append("bank_account", bank_account);
        formdata.append("ifsc", ifsc);
        formdata.append("aadhar_card", aadhar_card);
        formdata.append("pan_card", pan_card);
        formdata.append("user_photo", user_photo);
        formdata.append("address", address);
        formdata.append("skills", skills);
        formdata.append("contact_number", contact_number);
        // console.log(formdata);

        const response = await axios.post(`http://localhost:3001/${user.designation}/${user._id}`, formdata, {
            headers: {
                accept: "application/json",
                "Accept-Language": "en-US,en;q=0.8",
                "Content-Type": "multipart / form - data",
            }
        });

        navigate("/")

    };
    return (
        <div className="col-md-4 col-md-offset-4 center">
            <h2>Register:</h2> <br />
            <Form onSubmit={handleSubmit(handleRegister)}>
                <div className="mb-3">
                    <Label htmlfor="contact_number" className="form-label">
                        Contact_number
                    </Label>
                    <Input type="number" className="form-control" name="contact_number" id="contact_number"
                        placeholder="9876543210"
                        required maxLength={2} onChange={(e) => { setContact_number(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <Label for="skills" className="form-label">
                        Skills
                    </Label>
                    <Input type="text" className="form-control" name="skills" id="skills" required placeholder="Add skills" onChange={(e) => { setSkills(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <Label for="address" className="form-label">
                        Address
                    </Label>
                    <textarea type="text" placeholder="ex: 123,ahmedabad,gujarat - 393939" className="form-control" name="address" id="address" required onChange={(e) => { setAddress(e.target.value); }} />
                </div>

                <div className="mb-3">
                    <Label for="gender" className="form-label">
                        Gender
                    </Label>
                    <select className="form-select" aria-label="Default select example" name="gender" id="gender" value={gender} onChange={(e) => { setGender(e.target.value); }} required >
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>

                <div className="mb-3">
                    <Label for="DOB" className="form-label">
                        Birth Date
                    </Label>
                    <Input type="date" className="form-control" name="DOB" id="DOB" value={dob} onChange={(e) => { setDob(e.target.value); }} required />
                </div>

                <div className="mb-3">
                    <Label for="bank_name" className="form-label">
                        Bank name
                    </Label>
                    <Input type="text" placeholder="Enter Bank name" className="form-control" name="bank_name" id="bank_name" onChange={(e) => { setBank_name(e.target.value); }} required />
                </div>

                <div className="mb-3">
                    <Label for="bank_account" className="form-label">
                        Bank account
                    </Label>
                    <Input type="number" className="form-control" name="bank_account" placeholder="98398393983983939838983839" id="bank_account" onChange={(e) => { setBank_account(e.target.value); }} required />
                </div>

                <div className="mb-3">
                    <Label for="ifsc" className="form-label">
                        IFSC
                    </Label>
                    <Input type="text" placeholder="xtz9838983" className="form-control" name="ifsc" id="ifsc" onChange={(e) => { setIfsc(e.target.value); }} required />
                </div>

                <div className="mb-3">
                    <Label for="aadhar_card" className="form-label">
                        Aadhar Card
                    </Label>
                    <Input className="form-control" type="file" id="aadhar_card" name="aadhar_card" onChange={(e) => { setAadhar_card(e.target.files[0]) }} required />
                </div>

                <div className="mb-3">
                    <Label for="pan_card" className="form-label">
                        Pan Card
                    </Label>
                    <Input className="form-control" type="file" id="pan_card" name="pan_card" onChange={(e) => { setPan_card(e.target.files[0]); }} required />
                </div>

                <div className="mb-3">
                    <Label for="user_photo" className="form-label">
                        User Photo
                    </Label>
                    <Input className="form-control" type="file" id="user_photo" name="user_photo" onChange={(e) => { setUser_photo(e.target.files[0]); }} required />
                </div>

                <input type="submit" className="btn btn-outline-primary" />
            </Form>
            {/* </form> */}
        </div>
    );
};

export default UserRegistation;