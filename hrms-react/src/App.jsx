import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './App.css';
import './components/UserRegistation.css';
import { BrowserRouter, Routes, Route, Navigate, NavLink, Outlet } from 'react-router-dom'
import UserRegistation from './components/UserRegistation';
import Leave from './components/Leave';
import Task from './components/Task'
import Login from './components/Login';
import Welcome from './components/Welcome';
import Error from './components/Error';
import AddEmployee from './components/AddEmployee';

function App() {
  // console.log(user)
  return (
    // <div>
    //  <UserRegistation />
    // </div>
    <BrowserRouter>
      <div >

        {/* <Leave />  */}
        {/* <Task /> */}
        {/* <Login /> */}
        <Routes>
          <Route exact path="/" element={<Login />} />
          <Route exact path="/admin/welcome" element={<Welcome />} />
          <Route exact path="/employee/welcome" element={<Welcome />} />
          <Route exact path="/project_manager/welcome" element={<Welcome />} />
          <Route exact path="/account_manager/welcome" element={<Welcome />} />
          <Route exact path="/admin/register" element={<UserRegistation />} />
          <Route exact path="/employee/register" element={<UserRegistation />} />
          <Route exact path="/project_manager/register" element={<UserRegistation />} />
          <Route exact path="/account_manager/register" element={<UserRegistation />} />
          <Route exact path="/admin/add" element={<AddEmployee />} />
          <Route exact path="/error" element={<Error />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
