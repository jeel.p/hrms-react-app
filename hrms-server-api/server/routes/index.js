import User from "../models/User"
// import Task from "../models/Task"
import Project from "../models/Project"
import express from "express";
import emp_route from './employee';
import admin_route from './admin';

var router = express.Router();

router.use('/employee', emp_route);


router.use('/admin', admin_route);


// router.use('/employee', emp_route);


// router.use('/employee', emp_route);
// export default router 

/* GET home page. */
// router.get('/', function (req, res, next) {
//   res.render('index', { title: 'Express' });
// });

// router.get("/show", async (req, res) => {
//   let u = await User.findOne({ name: "anjali" })
//   res.render("show", { u, service })
// })

// router.post("/task", async (req, res) => {
//   const { description } = req.body;
//   let task = new Task({ user_id: 1, date: Date.now(), description: description })
//   await task.save();

// })

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  try {
    const person = await User.findOne({ email: email })
    if (person.password == password) {
      res.json({ status: true, person: person })
    }
    else {
      res.json({ status: false })
    }
  }
  catch (e) {
    res.json({ status: false })
  }
})

router.post("/project", async (req, res) => {
  const { project } = req.body;
  let p = new Project({});

})


export default router;