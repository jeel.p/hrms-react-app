import express from 'express';
import controller from './controller';
import multer from 'multer';
import path from 'path';

const routes = express.Router(); // eslint-disable-line new-cap


var upload = (path) => multer({
    storage: multerStorage(path)
});

let documents_path = path.join(__dirname, "../../../public/uploads/documents")

const multerStorage = (path) => multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, documents_path);
    },
    filename: (req, file, cb) => {
        let len = file.originalname.split(".").length;
        var fname = '-' + Date.now() + "." + file.originalname.split(".")[len - 1];
        cb(null, file.fieldname + fname)
    }
});

let cpUpload = upload(documents_path).fields([{ name: "aadhar_card", maxCount: 1 }, { name: "pan_card", maxCount: 1 }, { name: "user_photo", maxCount: 1 }]);

routes.post('/:id',cpUpload,controller.addUserDetail);



export default routes;