import User from "../../models/User"
import Task from "../../models/Task"
import Project from "../../models/Project"
import multer from 'multer';
import path from 'path';
import express from "express";

// exports.getUser = (async (req, res) => {
//   modal.find().lean(true).then((result) => {
//     res.json({ response: true, result: result });
//   }).catch(err => res.status(500).json(err));
// });



const addUserDetail = (async (req, res) => {
    const { id } = req.params;

    const { name, email, password, DOB, contact_number, skills, address, designation, gender, joining_date, bank_account, bank_branch, bank_name } = req.body;
    
    const person = await User.findByIdAndUpdate(id, { $set: { name: name, email: email, password: password, DOB: DOB, contact_number: contact_number, skills: skills, address: address, designation: designation, gender: gender, joining_date: joining_date, bank_account: bank_account, aadhar_card: "/uploads/documents/" + req.files.aadhar_card[0].filename, pan_card: "/uploads/documents/" + req.files.pan_card[0].filename, user_photo: "/uploads/documents/" + req.files.user_photo[0].filename, bank_branch: bank_branch, bank_name: bank_name } }, { runValidators: true, new: true })

    res.send("Added")
})

// const getById = [(req, res) => {
//   modal.findById({ _id: req.params.id }).lean(true).then((result) => {
//     res.json({ response: true, result: result });
//   }).catch(err => res.status(500).json(err));;
// }
// ]

// const addUser = [
//   (req, res) => {
//     modal.findOne({ email: req.body.email }).then(user => {
//       if (user) {
//         res.json({ msg: 'Email already exists' });
//       } else {
//         modal.create(req.body).then((result) => {
//           res.json({ response: true, result: result });
//         }).catch(err => res.status(500).json(err));
//       }
//     })
//   }
// ]

// const updateUser = [
//   (req, res) => {
//     modal.findByIdAndUpdate({ _id: req.params.id }, req.body).then((result) => {
//       modal.findById(req.params.id).then((resDate) => {
//         res.json({ response: true, result: resDate });
//       })
//     }).catch(err => res.status(500).json(err));
//   }
// ]

// const delUser = [(req, res) => {
//   modal.findByIdAndDelete({ _id: req.params.id }, req.body).then((result) => {
//     res.json({ response: true, result: result });
//   }).catch(err => res.status(500).json(err));
// }
// ]


// module.exports = {
//   getUser,
//   getById,
//   addUser,
//   updateUser,
//   delUser
// };


const controller = {
    addUserDetail: addUserDetail
};

export default controller;