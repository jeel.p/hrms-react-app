import User from "../../models/User"
import Task from "../../models/Task"
import Project from "../../models/Project"
import multer from 'multer';
import path from 'path';
import express from "express";

// exports.getUser = (async (req, res) => {
//   modal.find().lean(true).then((result) => {
//     res.json({ response: true, result: result });
//   }).catch(err => res.status(500).json(err));
// });



const addUserDetail = (async (req, res) => {

    const { id } = req.params;

    const { name, email, password, DOB, contact_number, skills, address, designation, gender, joining_date, bank_account, bank_branch, bank_name } = req.body;

    const person = await User.findByIdAndUpdate(id, { $set: { name: name, email: email, password: password, DOB: DOB, contact_number: contact_number, skills: skills, address: address, designation: designation, gender: gender, joining_date: joining_date, bank_account: bank_account, aadhar_card: "/uploads/documents/" + req.files.aadhar_card[0].filename, pan_card: "/uploads/documents/" + req.files.pan_card[0].filename, user_photo: "/uploads/documents/" + req.files.user_photo[0].filename, bank_branch: bank_branch, bank_name: bank_name } }, { runValidators: true, new: true })

    res.send("Added")
})


const addUser = (async (req, res) => {

    const { name, email, password, designation, joining_date } = req.body;

    let person = new User({ name: name, email: email, password: password, designation: designation, joining_date: joining_date })

    await person.save();

    res.send("Added")
})

const showUser = (async (req, res) => {

    const users = await User.find({})
    console.log("in show user")

    // await person.save();

    res.json(users)
})

const controller = {
    addUserDetail: addUserDetail,
    addUser: addUser,
    showUser: showUser
};

export default controller;