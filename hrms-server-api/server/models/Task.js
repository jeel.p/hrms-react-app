import mongoose from 'mongoose';
const TaskSchema = new mongoose.Schema({

    date: {
        type: Date,
        required: true
    },
    user_id: {
        // type: mongoose.Types.ObjectId,
        type: String,
        ref: "User",
        required: true
    },
    description: {
        type: String,
        required: true
    }

})
const Task = mongoose.model("task", TaskSchema)
export default Task;