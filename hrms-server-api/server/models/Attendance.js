import mongoose from 'mongoose';
const AttendanceSchema = new mongoose.Schema({
    user_id: {
        type: mongoose.Types.ObjectID,
        ref: "User",
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    status:{
        type:Boolean,
        required: true
    }
})
const Attendance=mongoose.model("attendance",AttendanceSchema)
export default Attendance;