import mongoose from 'mongoose';
const LeaveSchema=mongoose.Schema({
    user_id:{
        type:mongoose.Types.ObjectID,
        ref: "User",
        required:true
    },
    start_date:{
        type:Date,
        required:true
    },
    end_date:{
        type:Date,
        required:true
    },
    reason:{
        type:String,
        required:true
    },
    applied_date:{
        type:Date,
        required:true
    },
    tital:{
        type:String,
        required:true
    },
    admin_response:{
        type:String,
        required:true,
        enum:["pending","approved","denied"]
    },
    type:{
        type:String,
        required:true
    }
})
const Leave=mongoose.model("leave",LeaveSchema)
export default Leave;