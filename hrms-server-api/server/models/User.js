import mongoose from 'mongoose';
var UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    DOB: {
        type: Date,
    },
    contact_number: {
        type: Number,
    },
    skills: {
        type: String
    },
    address: {
        type: String
    },
    designation: {
        type: String,
        required: true,
        enum: ["admin", "employee", "project_manager", "account_manager"]
    },
    gender: {
        type: String,
        enum: ["male", "female"]
    },
    joining_date: {
        type: Date,
        required: true
    },
    bank_account: {
        type: Number
    },
    aadhar_card: {
        type: String
    },
    pan_card: {
        type: String
    },
    user_photo: {
        type: String
    },
    ifsc: {
        type: Number
    },
    bank_name: {
        type: String
    }
})
const User = mongoose.model("user", UserSchema)
export default User;