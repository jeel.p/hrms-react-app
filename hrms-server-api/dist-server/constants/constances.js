"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.service = void 0;
// var host = () => {
//     if (window.location.origin.startsWith("http://localhost:3000")) {
//         return {
//             prefix: "http://localhost:3000"
//         }
//     }
// }
// var hostname= host();
// export default hostname;
var service = {}; // Local

exports.service = service;
console.log("env", process.env);

if (process.env.NODE_ENV == "development") {
  service.API_URL = "http://localhost:3001";
} else {
  service.API_URL = "https://api-hashtag.pyther.com/api/";
}