"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var connectMongo = function connectMongo() {
  _mongoose["default"].connect('mongodb://localhost:27017/demo').then(function () {
    console.log("Connected successfully");
  })["catch"](function (e) {
    console.log("Something went wrong");
    console.log(e);
  });
};

var _default = connectMongo;
exports["default"] = _default;