"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _controller = _interopRequireDefault(require("./controller"));

var _multer = _interopRequireDefault(require("multer"));

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var routes = _express["default"].Router(); // eslint-disable-line new-cap


var upload = function upload(path) {
  return (0, _multer["default"])({
    storage: multerStorage(path)
  });
};

var documents_path = _path["default"].join(__dirname, "../../../public/uploads/documents");

var multerStorage = function multerStorage(path) {
  return _multer["default"].diskStorage({
    destination: function destination(req, file, cb) {
      cb(null, documents_path);
    },
    filename: function filename(req, file, cb) {
      var len = file.originalname.split(".").length;
      var fname = '-' + Date.now() + "." + file.originalname.split(".")[len - 1];
      cb(null, file.fieldname + fname);
    }
  });
};

var cpUpload = upload(documents_path).fields([{
  name: "aadhar_card",
  maxCount: 1
}, {
  name: "pan_card",
  maxCount: 1
}, {
  name: "user_photo",
  maxCount: 1
}]);
routes.post('/add', _controller["default"].addUser);
routes.post('/:id', cpUpload, _controller["default"].addUserDetail);
routes.get("/show", _controller["default"].showUser); // routes.get('/:id',
// controller.getById);
// routes.post('/',
// controller.addUser);
// routes.put('/:id',
// controller.updateUser);
// routes.delete('/:id',
// controller.delUser);

var _default = routes;
exports["default"] = _default;