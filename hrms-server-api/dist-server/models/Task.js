"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TaskSchema = new _mongoose["default"].Schema({
  date: {
    type: Date,
    required: true
  },
  user_id: {
    // type: mongoose.Types.ObjectId,
    type: String,
    ref: "User",
    required: true
  },
  description: {
    type: String,
    required: true
  }
});

var Task = _mongoose["default"].model("task", TaskSchema);

var _default = Task;
exports["default"] = _default;