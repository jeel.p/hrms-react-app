"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ProjectSchema = new _mongoose["default"].Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  start_date: {
    type: Date
  },
  project_members: {
    type: [_mongoose["default"].Types.ObjectId],
    ref: "User"
  },
  status: {
    type: String
  },
  end_date: {
    type: Date
  },
  deadline: {
    type: Date
  },
  estimated_duration: {
    type: Number
  }
});

var Project = _mongoose["default"].model("project", ProjectSchema);

var _default = Project;
exports["default"] = _default;