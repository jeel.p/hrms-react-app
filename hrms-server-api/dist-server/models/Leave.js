"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var LeaveSchema = _mongoose["default"].Schema({
  user_id: {
    type: _mongoose["default"].Types.ObjectID,
    ref: "User",
    required: true
  },
  start_date: {
    type: Date,
    required: true
  },
  end_date: {
    type: Date,
    required: true
  },
  reason: {
    type: String,
    required: true
  },
  applied_date: {
    type: Date,
    required: true
  },
  tital: {
    type: String,
    required: true
  },
  admin_response: {
    type: String,
    required: true,
    "enum": ["pending", "approved", "denied"]
  },
  type: {
    type: String,
    required: true
  }
});

var Leave = _mongoose["default"].model("leave", LeaveSchema);

var _default = Leave;
exports["default"] = _default;