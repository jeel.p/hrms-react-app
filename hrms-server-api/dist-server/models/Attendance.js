"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var AttendanceSchema = new _mongoose["default"].Schema({
  user_id: {
    type: _mongoose["default"].Types.ObjectID,
    ref: "User",
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  status: {
    type: Boolean,
    required: true
  }
});

var Attendance = _mongoose["default"].model("attendance", AttendanceSchema);

var _default = Attendance;
exports["default"] = _default;