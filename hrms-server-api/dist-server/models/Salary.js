"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var SalarySchema = _mongoose["default"].Schema({
  base_salary: {
    type: Number,
    "default": 0
  },
  bonus: {
    type: Number,
    "default": 0
  },
  allowance: {
    type: Number,
    "default": 0
  },
  user_id: {
    type: _mongoose["default"].Types.ObjectId,
    ref: "User"
  }
});

var Salary = _mongoose["default"].model("salary", SalarySchema);

var _default = Salary;
exports["default"] = _default;