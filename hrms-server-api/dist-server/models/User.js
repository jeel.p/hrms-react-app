"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var UserSchema = new _mongoose["default"].Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  DOB: {
    type: Date
  },
  contact_number: {
    type: Number
  },
  skills: {
    type: String
  },
  address: {
    type: String
  },
  designation: {
    type: String,
    required: true,
    "enum": ["admin", "employee", "project_manager", "account_manager"]
  },
  gender: {
    type: String,
    "enum": ["male", "female"]
  },
  joining_date: {
    type: Date,
    required: true
  },
  bank_account: {
    type: Number
  },
  aadhar_card: {
    type: String
  },
  pan_card: {
    type: String
  },
  user_photo: {
    type: String
  },
  ifsc: {
    type: Number
  },
  bank_name: {
    type: String
  }
});

var User = _mongoose["default"].model("user", UserSchema);

var _default = User;
exports["default"] = _default;